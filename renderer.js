// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
// console.log('log ze jest')

var back_message = { "instructions": "", "failure": "", "value": "" };
var max_oil = 70;
var max_coolant = 110

function prepand_failure_data(failure, value, obj) {
  $("h4").text('Failure ' + failure + ' = ' + value);
  $("tbody").prepend('<tr class="table-danger"><td class="col-3">' + obj['time'] + '</td> <td class="col-3">' + obj['oil'] +
    '</td> <td class="col-3">' + obj['coolant'] + '</td> <td class="col-3">' + obj['brakes'] + '</td></tr>');
  back_message["failure"] = failure
  back_message["value"] = value
  console.log(back_message)
}

function prepand_good_data(obj) {
  $("tbody").prepend('<tr><td class="col-3">' + obj['time'] + '</td> <td class="col-3">' + obj['oil'] +
    '</td> <td class="col-3">' + obj['coolant'] + '</td> <td class="col-3">' + obj['brakes'] + '</td></tr>')
}

ipcRenderer.on("data", function (event, param) {
  // console.log('meesage on header')
  // $("h4").text("new text")
  var obj = $.parseJSON(param);
  if (obj['oil'] > max_oil) {
    prepand_failure_data('oil', obj['oil'], obj)
  }
  else if (obj['coolant'] > max_coolant) {
    prepand_failure_data('coolant', obj['coolant'], obj)
  }
  else if (obj['brakes'] != 'OK') {
    prepand_failure_data('brakes', obj['brakes'])
  }
  else {
    prepand_good_data()
  }
});

$("#call_chief").click(function () {
  ipcRenderer.send('call_chief', back_message)
});
$("#call_driver").click(function () {
  back_message["instructions"] = "DRIVE TO PITSTOP"
  ipcRenderer.send('call_driver', back_message)
});




